terraform {
 required_providers {
   aws = {
     source  = "hashicorp/aws"
     version = "~> 3.0"
   }
 }
}

provider "aws" {
 region = "us-east-1"
}


resource "aws_kms_key" "terraform-bucket-key" {
 description             = "This key is used to encrypt bucket objects"
 deletion_window_in_days = 10
 enable_key_rotation     = true
}

resource "aws_kms_alias" "key-alias" {
 name          = "alias/terraform-bucket-key1"
 target_key_id = aws_kms_key.terraform-bucket-key.key_id
}

resource "aws_s3_bucket" "terraform-state" {
 bucket = "terraform-state-aclemons23"
 acl    = "private"

 versioning {
   enabled = true
 }

 server_side_encryption_configuration {
   rule {
     apply_server_side_encryption_by_default {
       kms_master_key_id = aws_kms_key.terraform-bucket-key.arn
       sse_algorithm     = "aws:kms"
     }
   }
 }
}

resource "aws_s3_bucket_public_access_block" "block" {
 bucket = aws_s3_bucket.terraform-state.id

 block_public_acls       = true
 block_public_policy     = true
 ignore_public_acls      = true
 restrict_public_buckets = true
}

resource "aws_dynamodb_table" "terraform-locks" {
 name           = "terraform-state-locking"
 read_capacity  = 20
 write_capacity = 20
 hash_key       = "LockID"
 attribute {
    name = "LockID"
    type = "S"
 } 

}

// -----------------------------------------------
// Fetch latest amazon 2 linux ami
// -----------------------------------------------
data "aws_ami" "linux2" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}
// -----------------------------------------------
// Fetch VPC id to use
// -----------------------------------------------
data "aws_vpc" "Main-Prod-Vpc" {
  filter {
    name   = "tag:Name"
    values = ["Main-Prod-Vpc"]
  }
}
// -----------------------------------------------
// Create instance profile, role and policy
// -----------------------------------------------
resource "aws_iam_role" "ec2-s3_role" {
  name = "ec2-s3_role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
resource "aws_iam_instance_profile" "ec2_profile" {
  name = "ec2c_profile"
  role = aws_iam_role.ec2-s3_role.name
}
resource "aws_iam_role_policy" "wps3_policy" {
  name = "mys3_policy"
  role = aws_iam_role.ec2-s3_role.id
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}
// -----------------------------------------------
// Create Web and RDS SG
// -----------------------------------------------
resource "aws_security_group" "web" {
  name        = "vpc_web"
  description = "Allow incoming HTTP connections."
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  vpc_id = data.aws_vpc.Main-Prod-Vpc.id
  tags = {
    Name = "WebServerSG"
  }
}
resource "aws_security_group" "db" {
  name        = "vpc_db"
  description = "Allow incoming database connections."
  ingress { # MySQL
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = [aws_security_group.web.id]
  }
  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  vpc_id = data.aws_vpc.Main-Prod-Vpc.id
  tags = {
    Name = "DBServerSG"
  }
}
// -----------------------------------------------
// Change USERDATA varible value after grabbing RDS endpoint info
// -----------------------------------------------
data "template_file" "user_data" {
  template = file("userdata.sh")
  vars = {
    db_username      = var.database_user
    db_user_password = var.database_password
    db_name          = var.database_name
    db_RDS           = aws_db_instance.wordpressdb.endpoint
  }
}
// -----------------------------------------------
// Create EC2 ( only after RDS is provisioned)
// -----------------------------------------------
resource "aws_instance" "wordpress" {
  ami                    = data.aws_ami.linux2.id
  availability_zone      = "us-east-1a"
  instance_type          = var.instance_type
  iam_instance_profile   = aws_iam_instance_profile.ec2_profile.name
  vpc_security_group_ids = [aws_security_group.web.id]
  user_data              = data.template_file.user_data.rendered
  associate_public_ip_address = true
  key_name                    = "Bastion"
  tags = {
    Name = "Wordpress.web"
  }
  depends_on = [aws_db_instance.wordpressdb]
}
// -----------------------------------------------
// Create RDS instance 
// -----------------------------------------------
resource "aws_db_instance" "wordpressdb" {
  allocated_storage = 10
  engine            = "mysql"
  engine_version    = "5.7"
  instance_class    = var.instance_class
  vpc_security_group_ids = ["${aws_security_group.db.id}"]
  name                   = var.database_name
  username               = var.database_user
  password               = var.database_password
  skip_final_snapshot    = true
}
// -----------------------------------------------
// Information to output
// -----------------------------------------------
#output "INFO" {
#  value = "AWS Resources and Wordpress has been provisioned. Go to http://${aws_instance.public_ip}"
#}