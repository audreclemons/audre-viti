// -----------------------------------------------
// Variables to use
// -----------------------------------------------
variable "database_name" {}
variable "database_user" {}
variable "database_password" {}
variable "instance_type" {}
variable "instance_class" {}